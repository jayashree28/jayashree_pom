package testcases;

import org.openqa.selenium.By;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.WebDriverWait;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import wdMethods.ProjectMethods;

public class TC004DeleteLead extends ProjectMethods {
	
	@BeforeTest(groups= {"any"})
	public void setData() {
		testCaseName = "TC002DeleteLead";
		testDesc = "Delete A Lead";
		author = "Jayashree";
		category = "smoke";
	}
	
	@Test
	public void deletelead() throws InterruptedException {
		
		WebElement eleleads = locateElement("linktext", "Leads");
		click(eleleads);
		WebElement eleFindLeads = locateElement("linktext", "Find Leads");
		click(eleFindLeads);
		WebElement elePhone = locateElement("linktext", "Phone");
		click(elePhone);
		WebElement ele_phoneno = locateElement("xpath", "//input[@name='phoneNumber']");
		type(ele_phoneno, "7708358530");
		WebElement eleFindLeads1 = locateElement("xpath", "(//button[@class='x-btn-text'])[7]");
		click(eleFindLeads1);
		Thread.sleep(1000);
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr/td/div/a"))).click();
		
		WebElement ele_delete = locateElement("linktext", "Delete");
		click(ele_delete);
		WebElement eleFindLeads_button = locateElement("xpath", "//ul[@class='shortcuts']/li[3]/a");
		click(eleFindLeads_button);
		closeBrowser();
		
	}
}
