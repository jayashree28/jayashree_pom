package wdMethods;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.openqa.selenium.By;
import org.openqa.selenium.NoSuchElementException;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.remote.RemoteWebDriver;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

import utils.Report;

public class SeMethods extends Report implements WdMethods{
	public int i = 1;
	public RemoteWebDriver driver;

	public void startApp(String browser, String url) {
		try {
			if (browser.equalsIgnoreCase	("chrome")) {			
				System.setProperty("webdriver.chrome.driver", "./drivers/chromedriver.exe");
				driver = new ChromeDriver();
			} 
//			else if (browser.equalsIgnoreCase("firefox")) {			
//				System.setProperty("webdriver.gecko.driver", "./drivers/geckodriver.exe");
//				driver = new FirefoxDriver();
//			}
			driver.get(url);
			driver.manage().window().maximize();
			driver.manage().timeouts().implicitlyWait(30, TimeUnit.SECONDS);
			//System.out.println("The Browser "+browser+" Launched Successfully");
			reportStep("Pass", "The Browser "+browser+" Launched Successfully");
		} catch (WebDriverException e) {
			//System.out.println("The Browser "+browser+" not Launched ");
			reportStep("fail", "The Browser "+browser+" not Launched ");
		} finally {
			takeSnap();			
		}
	}


	public WebElement locateElement(String locator, String locValue) {
		try {
			switch(locator) {
			case "id": return driver.findElementById(locValue);
			case "class": return driver.findElementByClassName(locValue);
			case "xpath": return driver.findElementByXPath(locValue);
			case "linktext" : return driver.findElementByLinkText(locValue);
			case "name" : return driver.findElementByName(locValue);
			
			}
			
		} catch (NoSuchElementException e) {
			//System.out.println("The Element Is Not Located ");
			reportStep("fail", "The Element is not located ");
		}
		return null;
	}

	@Override
	public WebElement locateElement(String locValue) {
		return driver.findElementById(locValue);
	}

	@Override
	public void type(WebElement ele, String data) {
		ele.clear();
		ele.sendKeys(data);
		//System.out.println("The Data "+data+" is Entered Successfully");
		reportStep("Pass", "The Data "+data+" is Entered Successfully");
		takeSnap();
	}

	@Override
	public void click(WebElement ele) {
		
		WebDriverWait wait = new WebDriverWait(driver, 10);
		wait.until(ExpectedConditions.elementToBeClickable(ele)).click();
		//ele.click();
		//System.out.println("The Element "+ele+" Clicked Successfully");
		reportStep("Pass", "The Element "+ele+" is clicked Successfully");
		//takeSnap();
	}

	@Override
	public String getText(WebElement ele) {
		String text = ele.getText();
		System.out.println("The captured text is: "+text);
		if(ele.getTagName().equalsIgnoreCase("table")) {
			WebElement table_element = driver.findElementByXPath("//table[@class='x-grid3-row-table']");
			List<WebElement> tr_collection = table_element.findElements(By.xpath("//table[@class='x-grid3-row-table']/tbody/tr"));
			System.out.println(tr_collection.size());
			for(int i = 0; i<=tr_collection.size(); i++)
			{
				List<WebElement> allColumns = tr_collection.get(i).findElements(By.tagName("td"));
				String text1 = allColumns.get(1).getText();
				System.out.println(text1);
			}
		}
		return text;
	}

	@Override
	public void selectDropDownUsingText(WebElement ele, String value) {
		Select dd = new Select(ele);
		dd.selectByVisibleText(value);
		System.out.println("The DropDown Is Selected with "+value);
	}

	@Override
	public void selectDropDownUsingIndex(WebElement ele, int index) {
		// TODO Auto-generated method stub

	}

	@Override
	public boolean verifyTitle(String expectedTitle) {
		
		return false;
		
	}

	@Override
	public void verifyExactText(WebElement ele, String expectedText) {
		if(expectedText.equals(ele.getText())) {
			System.out.println("The text is verified");
		}else {
			System.out.println("Text is not verified");
		}

	}

	@Override
	public void verifyPartialText(WebElement ele, String expectedText) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyExactAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyPartialAttribute(WebElement ele, String attribute, String value) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifySelected(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void verifyDisplayed(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void switchToWindow(int index) {
		Set<String> allWindows = driver.getWindowHandles();
		List<String> listOfWindow = new ArrayList<String>();
		listOfWindow.addAll(allWindows);
		driver.switchTo().window(listOfWindow.get(index));
		System.out.println("The Window is Switched ");
	}

	@Override
	public void switchToFrame(WebElement ele) {
		// TODO Auto-generated method stub

	}

	@Override
	public void acceptAlert() {
		driver.switchTo().alert().accept();
		//takeSnap();
	}

	@Override
	public void dismissAlert() {
		// TODO Auto-generated method stub

	}

	@Override
	public String getAlertText() {
		// TODO Auto-generated method stub
		return null;
	}

	@Override
	public void takeSnap() {
		try {
		File src = driver.getScreenshotAs(OutputType.FILE);
		File dsc = new File("./snaps/img"+i+".png");
			FileUtils.copyFile(src, dsc);
		} catch (IOException e) {
			e.printStackTrace();
		}
		i++;
	}

	@Override
	public void closeBrowser() {
		driver.close();

	}

	@Override
	public void closeAllBrowsers() {
		// TODO Auto-generated method stub

	}

}