package wdMethods;

import java.io.IOException;

import org.testng.annotations.Test;

import com.aventstack.extentreports.ExtentReports;
import com.aventstack.extentreports.ExtentTest;
import com.aventstack.extentreports.MediaEntityBuilder;
import com.aventstack.extentreports.Status;
import com.aventstack.extentreports.reporter.ExtentHtmlReporter;

public class LearnExtentReports {
	@Test
	public void name() throws IOException {
		ExtentHtmlReporter html = new ExtentHtmlReporter("./reports/results.html");
		html.setAppendExisting(false);
		ExtentReports extent = new ExtentReports();
		extent.attachReporter(html);	
		
		ExtentTest logger = extent.createTest("TC002CreateLead", "Create New Lead");
		logger.assignAuthor("Jayashree");
		logger.assignCategory("smoke");
		
		logger.log(Status.PASS, "The Data DemoSalesManager Entered Successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img2.png").build());
		
		logger.log(Status.PASS, "The Data crmsfa entered successfully", 
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img3.png").build());
		
		logger.log(Status.PASS, "The Login Button clicked successfully",
				MediaEntityBuilder.createScreenCaptureFromPath("./snaps/img4.png").build());
		
		extent.flush();
		
		
	}
}
