package week4.day2;

public class PrintMinInArray {

	public static void main(String[] args) {
		int[] a= {11,13,3,7,18,8,9};
		int minimum=a[0];
		for(int i=0;i<a.length;i++) {
			if(minimum>a[i]) {
				minimum=a[i];
			}
		}
		System.out.println(minimum);
	}
}
