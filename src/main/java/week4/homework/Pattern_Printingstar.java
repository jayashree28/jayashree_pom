package week4.homework;

import java.util.Scanner;

public class Pattern_Printingstar {

	public static void main(String[] args) {	
		Scanner s= new Scanner(System.in);
		System.out.println("Enter the range:");
		int range = s.nextInt();
		int k = 2*range - 2;
		for (int i=0; i<range; i++) 
        {
            for (int j=0; j<k; j++) {  
                System.out.print(" "); 
            }  
            k = k - 1;
            for (int j=0; j<=i; j++ ) {
            	System.out.print("* "); 
            }  
            System.out.println(); 
        } 
	}
}
