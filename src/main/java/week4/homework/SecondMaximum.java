package week4.homework;

public class SecondMaximum {

	public static void main(String[] args) {
		int[] a= {11,20,25,85,3,15,7};
		int minimum=a[0],temp=0,sec_min=a[1];
		for(int i=0;i<a.length;i++) {
			if(minimum>a[i]) {
				a[i]=minimum;
				temp=minimum;
			}
		}
		for(int j=1;j<a.length-1;j++) {
			if(sec_min>a[j]) {
				sec_min=a[j];
			}
		}
		System.out.println(sec_min);
	}

}
